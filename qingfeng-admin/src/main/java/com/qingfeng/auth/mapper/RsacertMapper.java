package com.qingfeng.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qingfeng.auth.entity.Rsacert;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @ProjectName RsacertMapper
 * @author Administrator
 * @version 1.0.0
 * @Description RsacertMapper
 * @createTime 2022/1/19 0019 22:54
 */
public interface RsacertMapper extends BaseMapper<Rsacert> {

    //查询数据分页列表
    IPage<Rsacert> findListPage(Page page, @Param("obj") Rsacert rsacert);

    //查询数据列表
    List<Rsacert> findList(Rsacert rsacert);

}