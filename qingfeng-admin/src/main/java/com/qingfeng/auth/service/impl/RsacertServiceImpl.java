package com.qingfeng.auth.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qingfeng.base.entity.QueryRequest;
import com.qingfeng.auth.entity.Rsacert;
import com.qingfeng.auth.mapper.RsacertMapper;
import com.qingfeng.auth.service.IRsacertService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @ProjectName RsacertServiceImpl
 * @author Administrator
 * @version 1.0.0
 * @Description RsacertServiceImpl接口
 * @createTime 2022/1/19 0019 22:55
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class RsacertServiceImpl extends ServiceImpl<RsacertMapper, Rsacert> implements IRsacertService {


    /**
     * @ProjectName RsacertServiceImpl
     * @author Administrator
     * @version 1.0.0
     * @Description 查询数据分页列表
     * @createTime 2022/1/19 0019 22:55
     */
    public IPage<Rsacert> findListPage(Rsacert rsacert, QueryRequest request){
        Page<Rsacert> page = new Page<>(request.getPageNum(), request.getPageSize());
        return this.baseMapper.findListPage(page, rsacert);
    }

    /**
     * @ProjectName RsacertServiceImpl
     * @author Administrator
     * @version 1.0.0
     * @Description 查询数据列表
     * @createTime 2022/1/19 0019 22:55
     */
    public List<Rsacert> findList(Rsacert rsacert){
        return this.baseMapper.findList(rsacert);
    }


}