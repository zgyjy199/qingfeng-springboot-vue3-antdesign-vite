package com.qingfeng.screen.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qingfeng.base.entity.QueryRequest;
import com.qingfeng.screen.entity.VisualMap;
import com.qingfeng.screen.mapper.VisualMapMapper;
import com.qingfeng.screen.service.IVisualMapService;
import com.qingfeng.utils.DateTimeUtil;
import com.qingfeng.utils.GuidUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @ProjectName GroupServiceImpl
 * @author qingfeng
 * @version 1.0.0
 * @Description TODO
 * @createTime 2021/4/3 0003 21:33
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class VisualMapServiceImpl extends ServiceImpl<VisualMapMapper, VisualMap> implements IVisualMapService {


    /**
     * @title findListPage
     * @description 查询数据分页列表
     * @author qingfeng
     * @updateTime 2021/4/3 0003 21:33
     */
    public IPage<VisualMap> findListPage(VisualMap visualMap, QueryRequest request){
        Page<VisualMap> page = new Page<>(request.getPageNum(), request.getPageSize());
        return this.baseMapper.findListPage(page, visualMap);
    }

    /**
     * @title findList
     * @description 查询数据列表
     * @author qingfeng
     * @updateTime 2021/4/3 0003 21:33
     */
    public List<VisualMap> findList(VisualMap visualMap){
        return this.baseMapper.findList(visualMap);
    }

    /**
     * @title saveGroup
     * @description 保存数据
     * @author qingfeng
     * @updateTime 2021/4/3 0003 21:33
     */
    public void saveVisualMap(VisualMap visualMap){
        // 创建用户
        String id = GuidUtil.getUuid();
        visualMap.setId(id);
        String time = DateTimeUtil.getDateTimeStr();
        visualMap.setCreateTime(time);
        //处理数据权限
        visualMap.setCreateUser("1");
        visualMap.setCreateOrganize("1");
        this.save(visualMap);
    }

    /**
     * @title updateGroup
     * @description 更新数据
     * @author qingfeng
     * @updateTime 2021/4/3 0003 21:33
     */
    public void updateVisualMap(VisualMap visualMap){
        // 更新组织信息
        String time = DateTimeUtil.getDateTimeStr();
        visualMap.setUpdateTime(time);
        visualMap.setUpdateUser("1");
        this.updateById(visualMap);
    }


}