package com.qingfeng.screen.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.qingfeng.base.entity.QueryRequest;
import com.qingfeng.screen.entity.Visual;
import com.qingfeng.screen.entity.VisualDTO;

import java.util.List;

/**
 * @ProjectName IVisualService
 * @author Administrator
 * @version 1.0.0
 * @Description TODO
 * @createTime 2021/4/17 0017 13:32
 */
public interface IVisualService extends IService<Visual> {

    //查询数据分页列表
    IPage<Visual> findListPage(Visual visual, QueryRequest request);

    //查询数据列表
    List<Visual> findList(Visual visual);

    //保存信息
    boolean saveVisual(VisualDTO visualDTO);

    //更新信息
    boolean updateVisual(VisualDTO visualDTO);

    VisualDTO detail(Visual visual);

    String copyVisual(String id);


}