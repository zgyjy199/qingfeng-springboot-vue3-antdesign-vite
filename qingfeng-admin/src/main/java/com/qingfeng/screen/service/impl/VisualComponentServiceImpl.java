package com.qingfeng.screen.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qingfeng.base.entity.QueryRequest;
import com.qingfeng.screen.entity.VisualComponent;
import com.qingfeng.screen.entity.VisualMap;
import com.qingfeng.screen.mapper.VisualComponentMapper;
import com.qingfeng.screen.mapper.VisualMapMapper;
import com.qingfeng.screen.service.IVisualComponentService;
import com.qingfeng.screen.service.IVisualMapService;
import com.qingfeng.utils.DateTimeUtil;
import com.qingfeng.utils.GuidUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @ProjectName VisualComponentServiceImpl
 * @author Administrator
 * @version 1.0.0
 * @Description VisualComponentServiceImpl
 * @createTime 2022/11/20 0020 0:05
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class VisualComponentServiceImpl extends ServiceImpl<VisualComponentMapper, VisualComponent> implements IVisualComponentService {


    /**
     * @title findListPage
     * @description 查询数据分页列表
     * @author qingfeng
     * @updateTime 2021/4/3 0003 21:33
     */
    public IPage<VisualComponent> findListPage(VisualComponent visualComponent, QueryRequest request){
        Page<VisualComponent> page = new Page<>(request.getPageNum(), request.getPageSize());
        return this.baseMapper.findListPage(page, visualComponent);
    }

    /**
     * @title findList
     * @description 查询数据列表
     * @author qingfeng
     * @updateTime 2021/4/3 0003 21:33
     */
    public List<VisualComponent> findList(VisualComponent visualComponent){
        return this.baseMapper.findList(visualComponent);
    }

    /**
     * @title saveGroup
     * @description 保存数据
     * @author qingfeng
     * @updateTime 2021/4/3 0003 21:33
     */
    public void saveVisualMap(VisualComponent visualComponent){
        // 创建用户
        String id = GuidUtil.getUuid();
        visualComponent.setId(id);
        String time = DateTimeUtil.getDateTimeStr();
        visualComponent.setCreateTime(time);
        //处理数据权限
        visualComponent.setCreateUser("1");
        visualComponent.setCreateOrganize("1");
        this.save(visualComponent);
    }

    /**
     * @title updateGroup
     * @description 更新数据
     * @author qingfeng
     * @updateTime 2021/4/3 0003 21:33
     */
    public void updateVisualMap(VisualComponent visualComponent){
        // 更新组织信息
        String time = DateTimeUtil.getDateTimeStr();
        visualComponent.setUpdateTime(time);
        visualComponent.setUpdateUser("1");
        this.updateById(visualComponent);
    }


}