package com.qingfeng.screen.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.qingfeng.base.entity.QueryRequest;
import com.qingfeng.screen.entity.VisualMap;

import java.util.List;

/**
 * @ProjectName IVisualMapService
 * @author Administrator
 * @version 1.0.0
 * @Description TODO
 * @createTime 2021/4/17 0017 13:32
 */
public interface IVisualMapService extends IService<VisualMap> {

    //查询数据分页列表
    IPage<VisualMap> findListPage(VisualMap visualMap, QueryRequest request);

    //查询数据列表
    List<VisualMap> findList(VisualMap visualMap);

    //保存信息
    void saveVisualMap(VisualMap visualMap);

    //更新信息
    void updateVisualMap(VisualMap visualMap);

}