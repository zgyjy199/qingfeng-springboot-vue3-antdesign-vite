package com.qingfeng.screen.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.qingfeng.base.entity.QueryRequest;
import com.qingfeng.screen.entity.VisualCategory;

import java.util.List;

/**
 * @ProjectName IVisualCategoryService
 * @author Administrator
 * @version 1.0.0
 * @Description TODO
 * @createTime 2021/4/17 0017 13:33
 */
public interface IVisualCategoryService extends IService<VisualCategory> {

    //查询数据分页列表
    IPage<VisualCategory> findListPage(VisualCategory visualCategory, QueryRequest request);

    //查询数据列表
    List<VisualCategory> findList(VisualCategory visualCategory);

    //保存信息
    void saveVisualCategory(VisualCategory visualCategory);

    //更新信息
    void updateVisualCategory(VisualCategory visualCategory);

}