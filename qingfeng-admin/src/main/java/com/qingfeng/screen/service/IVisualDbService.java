package com.qingfeng.screen.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.qingfeng.base.entity.QueryRequest;
import com.qingfeng.screen.entity.VisualDb;

import java.util.List;

/**
 * @ProjectName IVisualService
 * @author Administrator
 * @version 1.0.0
 * @Description TODO
 * @createTime 2021/4/17 0017 13:32
 */
public interface IVisualDbService extends IService<VisualDb> {

    //查询数据分页列表
    IPage<VisualDb> findListPage(VisualDb visualDb, QueryRequest request);

    //查询数据列表
    List<VisualDb> findList(VisualDb visualDb);

    //保存信息
    boolean saveVisualDb(VisualDb visualDb);

    //更新信息
    boolean updateVisualDb(VisualDb visualDb);



}