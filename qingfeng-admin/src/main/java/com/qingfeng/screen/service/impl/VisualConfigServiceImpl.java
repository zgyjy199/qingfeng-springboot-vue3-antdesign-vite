package com.qingfeng.screen.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qingfeng.base.entity.QueryRequest;
import com.qingfeng.screen.entity.VisualConfig;
import com.qingfeng.screen.mapper.VisualConfigMapper;
import com.qingfeng.screen.service.IVisualConfigService;
import com.qingfeng.utils.DateTimeUtil;
import com.qingfeng.utils.GuidUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @ProjectName GroupServiceImpl
 * @author qingfeng
 * @version 1.0.0
 * @Description TODO
 * @createTime 2021/4/3 0003 21:33
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class VisualConfigServiceImpl extends ServiceImpl<VisualConfigMapper, VisualConfig> implements IVisualConfigService {


    /**
     * @title findListPage
     * @description 查询数据分页列表
     * @author qingfeng
     * @updateTime 2021/4/3 0003 21:33
     */
    public IPage<VisualConfig> findListPage(VisualConfig visualConfig, QueryRequest request){
        Page<VisualConfig> page = new Page<>(request.getPageNum(), request.getPageSize());
        return this.baseMapper.findListPage(page, visualConfig);
    }

    /**
     * @title findList
     * @description 查询数据列表
     * @author qingfeng
     * @updateTime 2021/4/3 0003 21:33
     */
    public List<VisualConfig> findList(VisualConfig visualConfig){
        return this.baseMapper.findList(visualConfig);
    }

    /**
     * @title saveGroup
     * @description 保存数据
     * @author qingfeng
     * @updateTime 2021/4/3 0003 21:33
     */
    public void saveVisualConfig(VisualConfig visualConfig){
        // 创建用户
        String id = GuidUtil.getUuid();
        visualConfig.setId(id);
        String time = DateTimeUtil.getDateTimeStr();
        visualConfig.setCreateTime(time);
        //处理数据权限
        visualConfig.setCreateUser("1");
        visualConfig.setCreateOrganize("1");
        this.save(visualConfig);
    }

    /**
     * @title updateGroup
     * @description 更新数据
     * @author qingfeng
     * @updateTime 2021/4/3 0003 21:33
     */
    public void updateVisualConfig(VisualConfig visualConfig){
        // 更新组织信息
        String time = DateTimeUtil.getDateTimeStr();
        visualConfig.setUpdateTime(time);
        visualConfig.setUpdateUser("1");
        this.updateById(visualConfig);
    }


}