package com.qingfeng.screen.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qingfeng.base.entity.QueryRequest;
import com.qingfeng.screen.entity.VisualCategory;
import com.qingfeng.screen.mapper.VisualCategoryMapper;
import com.qingfeng.screen.service.IVisualCategoryService;
import com.qingfeng.utils.DateTimeUtil;
import com.qingfeng.utils.GuidUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @ProjectName GroupServiceImpl
 * @author qingfeng
 * @version 1.0.0
 * @Description TODO
 * @createTime 2021/4/3 0003 21:33
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class VisualCategoryServiceImpl extends ServiceImpl<VisualCategoryMapper, VisualCategory> implements IVisualCategoryService {


    /**
     * @title findListPage
     * @description 查询数据分页列表
     * @author qingfeng
     * @updateTime 2021/4/3 0003 21:33
     */
    public IPage<VisualCategory> findListPage(VisualCategory visualCategory, QueryRequest request){
        Page<VisualCategory> page = new Page<>(request.getPageNum(), request.getPageSize());
        return this.baseMapper.findListPage(page, visualCategory);
    }

    /**
     * @title findList
     * @description 查询数据列表
     * @author qingfeng
     * @updateTime 2021/4/3 0003 21:33
     */
    public List<VisualCategory> findList(VisualCategory visualCategory){
        return this.baseMapper.findList(visualCategory);
    }

    /**
     * @title saveGroup
     * @description 保存数据
     * @author qingfeng
     * @updateTime 2021/4/3 0003 21:33
     */
    public void saveVisualCategory(VisualCategory visualCategory){
        // 创建用户
        String id = GuidUtil.getUuid();
        visualCategory.setId(id);
        String time = DateTimeUtil.getDateTimeStr();
        visualCategory.setCreateTime(time);
        visualCategory.setIsDeleted("0");
        //处理数据权限
        visualCategory.setCreateUser("1");
        visualCategory.setCreateOrganize("1");
        this.save(visualCategory);
    }

    /**
     * @title updateGroup
     * @description 更新数据
     * @author qingfeng
     * @updateTime 2021/4/3 0003 21:33
     */
    public void updateVisualCategory(VisualCategory visualCategory){
        // 更新组织信息
        String time = DateTimeUtil.getDateTimeStr();
        visualCategory.setUpdateTime(time);
        visualCategory.setUpdateUser("1");
        this.updateById(visualCategory);
    }


}