package com.qingfeng.screen.controller;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.qingfeng.base.controller.BaseController;
import com.qingfeng.base.entity.QueryRequest;
import com.qingfeng.framework.exception.BizException;
import com.qingfeng.screen.entity.Visual;
import com.qingfeng.screen.entity.VisualCategory;
import com.qingfeng.screen.entity.VisualDTO;
import com.qingfeng.screen.service.IVisualCategoryService;
import com.qingfeng.screen.service.IVisualService;
import com.qingfeng.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @ProjectName VisualController
 * @author Administrator
 * @version 1.0.0
 * @Description TODO
 * @createTime 2021/4/17 0017 14:58
 */
@Slf4j
@Validated
@RestController
@RequestMapping("visual")
public class VisualController extends BaseController {

    @Autowired
    private IVisualService visualService;
    @Autowired
    private IVisualCategoryService visualCategoryService;

    /**
     * @title findListPage
     * @description 查询数据分页列表
     * @author Administrator
     * @updateTime 2021/4/17 0017 14:51
     */
    @GetMapping("/list")
    public MyResponse findListPage(QueryRequest queryRequest, Visual visual) {
        String userParams = SecurityContextHolder.getContext().getAuthentication().getName();
        Map<String, Object> dataTable = MyUtil.getDataTable(visualService.findListPage(visual, queryRequest));
        return new MyResponse().data(dataTable);
    }

    /**
     * @title detail
     * @description 查询详情信息
     * @author Administrator
     * @updateTime 2021/4/17 0017 14:51
     */
    @GetMapping("/detail")
    public MyResponse detail(Visual visual) {
        VisualDTO detail = visualService.detail(visual);
        return new MyResponse().data(detail);
    }

    /**
     * @title findList
     * @description 查询信息列表
     * @author Administrator
     * @updateTime 2021/4/17 0017 14:51
     */
    @GetMapping("/findList")
    public MyResponse findList(Visual visual) {
        String userParams = SecurityContextHolder.getContext().getAuthentication().getName();
        List<Visual> mapList = visualService.findList(visual);
        return new MyResponse().data(mapList);
    }



    /**
     * @title save
     * @description 保存数据
     * @author Administrator
     * @updateTime 2021/4/17 0017 14:52
     */
    @PostMapping("/save")
    public MyResponse save(@Valid @RequestBody VisualDTO visualDTO) throws BizException {
        try {
            visualService.saveVisual(visualDTO);
            return new MyResponse().data(visualDTO.getVisual().getId());
        } catch (Exception e) {
            String message = "新增信息失败";
            log.error(message, e);
            throw new BizException(message);
        }
    }

    /**
     * @title update
     * @description 更新数据
     * @author Administrator
     * @updateTime 2021/4/17 0017 14:52
     */
    @PostMapping("/update")
    public void update(@Valid @RequestBody VisualDTO visualDTO) throws BizException {
        try {
            this.visualService.updateVisual(visualDTO);
        } catch (Exception e) {
            String message = "修改信息失败";
            log.error(message, e);
            throw new BizException(message);
        }
    }

    /**
     * @title delete
     * @description 删除数据
     * @author Administrator
     * @updateTime 2021/4/17 0017 14:52
     */
    @DeleteMapping("/{ids}")
    public void delete(@NotBlank(message = "{required}") @PathVariable String ids) throws BizException {
        try {
            String[] del_ids = ids.split(StringPool.COMMA);
            this.visualService.removeByIds(Arrays.asList(del_ids));
        } catch (Exception e) {
            String message = "删除失败";
            log.error(message, e);
            throw new BizException(message);
        }
    }

    @PostMapping("/copy")
    public MyResponse copy(@RequestParam String id) {
        return new MyResponse().data(visualService.copyVisual(id));
    }


    @GetMapping("category")
    public MyResponse category() {
        List<VisualCategory> list = visualCategoryService.list();
        return new MyResponse().data(list);
    }

    /**
     * 上传文件
     */
    @PostMapping("/put-file")
    public MyResponse putFile(@RequestParam MultipartFile file) throws Exception {
        PageData pd = new PageData();
        String filename = file.getOriginalFilename();// 文件原名称
        String fileSuffix = filename.substring(filename.lastIndexOf("."));//文件类型
        String fileType = filename.substring(filename.lastIndexOf(".")+1);//文件后缀
        //保存文件
        String savePath = ParaUtil.localName;
        String path = ParaUtil.common+ DateTimeUtil.getDate()+"/"+ GuidUtil.getGuid()+fileSuffix;
        File files = new File(savePath+path);
        if (!files.getParentFile().exists()){
            files.getParentFile().mkdirs();
        }
        file.transferTo(files);
        pd.put("name",filename);
        pd.put("desnames",filename);
        pd.put("file_path",path);
        pd.put("file_type",fileType);
        pd.put("file_size",files.length());
        pd.put("show_path",ParaUtil.cloudfile+path);
        return new MyResponse().data(pd);
    }




}
