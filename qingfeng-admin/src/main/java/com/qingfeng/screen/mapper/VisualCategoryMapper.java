package com.qingfeng.screen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qingfeng.screen.entity.VisualCategory;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @ProjectName qingfeng-cloud
 * @Description TODO
 * @createTime 2021年04月17日 12:13:00
 */
public interface VisualCategoryMapper extends BaseMapper<VisualCategory> {

    //查询数据分页列表
    IPage<VisualCategory> findListPage(Page page, @Param("obj") VisualCategory visualCategory);

    //查询数据列表
    List<VisualCategory> findList(VisualCategory visualCategory);

}