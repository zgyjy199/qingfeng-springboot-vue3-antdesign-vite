package com.qingfeng.screen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qingfeng.screen.entity.VisualMap;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @ProjectName qingfeng-cloud
 * @Description TODO
 * @createTime 2021年04月17日 12:17:00
 */
public interface VisualMapMapper extends BaseMapper<VisualMap> {

    //查询数据分页列表
    IPage<VisualMap> findListPage(Page page, @Param("obj") VisualMap visualMap);

    //查询数据列表
    List<VisualMap> findList(VisualMap visualMap);

}
