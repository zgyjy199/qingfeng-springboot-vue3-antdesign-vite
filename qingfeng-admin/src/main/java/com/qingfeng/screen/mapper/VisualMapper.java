package com.qingfeng.screen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qingfeng.screen.entity.Visual;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @ProjectName qingfeng-cloud
 * @Description TODO
 * @createTime 2021年04月17日 12:12:00
 */
public interface VisualMapper extends BaseMapper<Visual> {

    //查询数据分页列表
    IPage<Visual> findListPage(Page page, @Param("obj") Visual visual);

    //查询数据列表
    List<Visual> findList(Visual visual);

}
