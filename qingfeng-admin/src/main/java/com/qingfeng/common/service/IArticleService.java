package com.qingfeng.common.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.qingfeng.base.entity.QueryRequest;
import com.qingfeng.common.entity.Article;
import com.qingfeng.common.entity.Tdemo;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.ArrayList;
import java.util.List;

/**
 * @ProjectName ITdemoService
 * @author Administrator
 * @version 1.0.0
 * @Description ITdemoService接口
 * @createTime 2022/1/19 0019 22:55
 */
public interface IArticleService {

    /**
     * 添加文章
     * @param article
     * @return
     */
    public Article create(Article article);

    /**
     * 删除文章
     * @param id
     */
    public void delete(String id);

    /**
     * 查询详情
     * @param id
     * @return
     */
    public Article get(String id);

}